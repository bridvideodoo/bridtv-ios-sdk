//
//  Playlist.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/25/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface PlaylistData : Parser

@property (nonatomic, readonly, strong, nullable) NSString *_id;
@property (nonatomic, readonly, strong, nullable) NSString *name;
@property (nonatomic, readonly, strong, nullable) NSNumber *items;
@property (nonatomic, readonly, strong, nullable) NSNumber *page;
@property (nonatomic, readonly, strong, nullable) NSNumber *limit;
@property (nonatomic, readonly, strong, nullable) NSString *exclude_tag;

@end
