//
//  BVPlayer.h
//  BridSDK
//
//  Created by Marko on 4/4/19.
//  Copyright © 2019 Brid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BVData.h"
#import "BVAdData.h"

@interface BVPlayer: NSObject

- (instancetype _Nullable)init __deprecated __deprecated_msg("This will create empty player, without data.");
- (instancetype _Nullable)initWithData:(BVData *_Nonnull)baseData forView:(UIView *_Nullable)containerView;
- (instancetype _Nullable)initPrebid:(NSArray<AdData *> *_Nonnull)ads playerID:(int)player forVideoID:(int)video forView:(UIView *_Nullable)containerView;
- (instancetype _Nullable)initCustomVideoWithPrebid:(NSArray<AdData *> *_Nonnull)ads playerID:(int)player videoDataArray:(NSArray<VideoData *> *_Nullable)videoData forView:(UIView *_Nullable)containerView;
- (instancetype _Nullable)initSingleVideo:(VideoData*_Nonnull)video playerID:(int)player forView:(UIView *_Nonnull)containerView;
- (instancetype _Nullable)initPlaylist:(NSArray<VideoData *> *_Nullable)videoData withPlayerID:(int)player forView:(UIView *_Nonnull)containerView;

- (void)play;
- (void)pause;
- (void)mute;
- (void)unmute;
- (void)previous;
- (void)next;
- (void)stop;
- (void)autoFullscreenWhenRotating:(BOOL)rotate;
- (void)setToFullscreen;
- (void)showCloseButton;


// MARK: Get Methods
- (BOOL)isFullscreen;
- (BOOL)isPlayerInView;
- (double)getCurrentTime;
- (double)getDuration;
- (BOOL)getMuted;
- (float)getVolume;
- (BOOL)isAdInProgress;

- (NSInteger)getCurrentIndex;
- (NSArray<VideoData *> *_Nullable)getPlaylist;
- (NSString *_Nullable)getSource;
- (VideoData *_Nullable)getVideo;

// MARK: Set Methods
- (void)setVolume:(float)volume;
- (void)playByIndex:(NSInteger)index;
- (void)instantPlayVideo:(VideoData *_Nullable)video;
- (void)setVideo:(VideoData *_Nullable)video;
- (void)setVideoUrl:(NSString *_Nullable)url;
- (void)setPlaylist:(NSURL *_Nullable)url;
- (void)setAd:(NSArray<AdData *> *_Nullable)ad;
- (void)playAd:(AdData *_Nullable)ad;
- (void)playAdTagUrl:(NSURL *_Nullable)url;
- (void)setAdMacros:(AdMacros *_Nullable)macros;
- (void)autoHideControls:(BOOL)isHidden;
- (void)setPrebidForMidroll:(NSString *_Nullable)adTagUrl;
- (void)setPrebidForPostroll:(NSString *_Nullable)adTagUrl;

- (void)setContainerView:(UIView * _Nullable)containerView;
- (void)registerVideoOverlay:(UIView *_Nullable)adContainer companionSlots:(NSArray *_Nullable)companionSlots;

@property (readonly, strong, nonatomic) UIView *_Nullable adContainer;
@property (readonly, copy, nonatomic) NSArray *_Nullable companionSlots;

@end
