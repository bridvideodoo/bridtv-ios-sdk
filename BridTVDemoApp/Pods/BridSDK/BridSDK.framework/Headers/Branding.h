//
//  Branding.h
//  BridSDK
//
//  Created by Xcode Peca on 08/05/2020.
//  Copyright © 2020 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface Branding : Parser

@property (nonatomic, readonly, strong, nullable) NSString *img;
@property (nonatomic, readonly, strong, nullable) NSString *url;
@property (nonatomic, readonly, strong, nullable) NSString *pos;

- (instancetype _Nullable )initWithMobile:(NSString *_Nullable)img
                                 url:(NSString *_Nullable)url pos:(NSString *_Nullable)pos;

@end
