//
//  BVData.h
//  BridSDK
//
//  Created by Marko on 4/3/19.
//  Copyright © 2019 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BVObject.h"
#import "ConfigurationData.h"

typedef enum : NSUInteger
{
    BVDataTypeVideo,
    BVDataTypePlaylist,
    BVDataTypeLatest,
    BVDataTypeChannel,
    BVDataTypeTag,
    BVDataCompanyPlaylist
} BVDataType;

@interface BVData: BVObject
@property (nonatomic, readonly) int playerID;
@property (nonatomic, readonly) int typeID;
@property (nonatomic, readonly, nonnull) NSString *tagType;
@property (nonatomic, readonly) int page;
@property (nonatomic, readonly) int item;
@property (nonatomic, readonly) BVDataType type;
@property (nonatomic, readonly, nullable) NSArray<VideoData *> *videoData;
@property (nonatomic, readonly, nullable) ConfigurationData *configurationData;

- (instancetype _Nonnull)initPlayerID:(int)player forVideoID:(int)video;
- (instancetype _Nonnull)initPlayerID:(int)player forPlaylistID:(int)playlist;
- (instancetype _Nonnull)initPlayerID:(int)player forLatestID:(int)listID page:(int)page item:(int)item;
- (instancetype _Nonnull)initPlayerID:(int)player forChannelID:(int)channelID page:(int)page item:(int)item;
- (instancetype _Nonnull)initPlayerID:(int)player forTag:(NSString *_Nonnull)tag page:(int)page item:(int)item;
- (instancetype _Nonnull)initPlayerID:(int)player forVideoData:(NSArray<VideoData *> *_Nullable)videoData;
- (instancetype _Nonnull)initPlayerID:(int)player forCompanyPlaylist:(int)company;
- (instancetype _Nonnull)initPlayerID:(int)player forVideo:(NSString *_Nullable)url;
@end
