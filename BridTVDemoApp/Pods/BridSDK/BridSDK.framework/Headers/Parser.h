//
//  Parser.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/18/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject

@property (nonatomic, readonly, strong, nullable) NSDictionary *asDictionary;

- (instancetype _Nonnull)initWithJSON:(NSDictionary *_Nonnull) parser;
+ (id _Nonnull)createDataObject:(NSDictionary *_Nonnull)json forName:(NSString *_Nonnull)name;

@end
