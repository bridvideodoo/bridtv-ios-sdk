# BridSDK IOS Example

This SDK aims at easily playing videos with or without ads in your iOS application. Its was written in objective c, and supports swift and objective c programming language.  
Add pod 'BridSDK' to yours pod file.  
This example depends on GoogleAds-IMA-iOS-SDK, so it was inserted through pods.

* Dependency: GoogleAds-IMA-iOS-SDK

## Example application

Integration of the Brid SDK is very simple. 

BridSDK gives you the factory methods to pair the player with your player id that you set up in BRID CMS.
Each Player (BVPlayer), is created with the help of your credentials (which includes the ID of your player, playlist ID etc.
BVPlayer places itself within the Container view you have at your disposal, which is basically a UIView, it is now on you to:


1) Setup a BVObject data object where you can configure a player instance using your credentials


2) Give it a container view in which to display them

The container view (UIView) by itself will retain the @weak reference of BVPlayer view but not BVPlayer itself which in addition to the view contains logic and networking
so the player will not work unless stored as a @strong reference.

#### Example 1:

If you have one container view that is not reusable, create one ``var player = BVPlayer (…)``, and give player.setView (containerView)


#### Example 2: 

If you have reusable viewers (UITableView, UICollectionView), create an array ``var player: [BVPlayer]`` in which you save the players and in each preparation of a new reusable cell (UITableCell) re-adjust the view from the player list

The stop, pause, and use other commands, execute them on the BVPlayer object.

## You can choose the type of BVData:

### Player with single video

Swift:``BVData(playerID: Int32, forVideoID: Int32)`` 

Objc:``[[BVData alloc] initPlayerID:<int> forVideoID:<int>]``

### Player with playlist

Swift:``BVData(playerID: Int32, forPlaylistID: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forPlaylistID:<int>]``

### Player with playlist by channel and option from what page and how many to load


Swift:``BVData(playerID: Int32, forChanneltID: Int32, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forChanneltID:<int> page:<int> item:<int>]``

### Player with latest videos playlist and option from what page and how many to load

Swift:``BVData(playerID: Int32, forLatestID: Int32, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forLatestID:<int> page:<int> item:<int>]``

### Player with a video by tag playlist, it loads video with tag string and option from what page and how many to load

Swift:``BVData(playerID: Int32, forTag: String, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forTag:<String> page:<int> item:<int>]``

---

# How to use

### Declare a player variable:

### Swift:  
``var player: BVPlayer?``

### Objc:  
``@property (nonatomic, strong) BVPlayer player;``  

### Initialize player with object:

### Swift:  
``player = BVPlayer(data: BVData(playerID: <Int32>, forVideoID: <Int32>), for: <UIView>)``

### Objc:  
``player = [[BVPlayer alloc] initWithData:[[BVData alloc] initPlayerID:<int> forVideoID:<int>]  forView:<UIView>]``

### Listen to events from player:

You can listen to events of the player through the Notification Centre. There are player events and ad events. Just change the string in notification.usreInfo.
 
### Swift: 
```
func setupEventNetworking() {  
       NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "PlayerEvent"), object: nil) 
	   NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "AdEvent"), object: nil) 
}
@objc func eventWriter(_ notification: NSNotification) {       
	if notification.name.rawValue == "PlayerEvent" {
		print(notification.userInfo?["event"] as! String)
   	}  
 	if notification.name.rawValue == "AdEvent" {   
		print(notification.userInfo?["ad"] as! String)
    }   
}
```

### Objc:
```
- (void)setupEventNetworking {   
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventWriter:) name:@"PlayerEvent" object:nil];   
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventWriter:) name:@"AdEvent" object:nil];    
}  
- (void) eventWriter:(NSNotification *)notification {
	if ([notification.name isEqualToString:@"PlayerEvent"]) {  
    	[self logMessage:(NSString *)notification.userInfo[@"event"]]; 
	}  
	if ([notification.name isEqualToString:@"AdEvent"]) { 
    	[self logMessage:(NSString *)notification.userInfo[@"ad"]];  
	}  
}  
```

## BridSDK player example methods:

### Swift:	 
``player.play()`` 					
``player.pause()`` 				
``player.mute()`` 					
``player.unmute()``               
``player.next()``                     
``player.previous()``                                         
``player.stop()``                                  

### Objc:	  
``[self.player play];`` 			
``[self.player pause];`` 			
``[self.player mute];`` 			
``[self.player unmute];`` 		     
``[self.player next];`` 		     
``[self.player previous];`` 		     
``[self.player stop];`` 			     

## BridSDK player getter methods:

### Swift:  
``player.isFullscreen()``			
``player.isPlayerInView()`` 					
``player.getCurrentTime()`` 				
``player.getDuration()`` 					
``player.getMuted()``               
``player.getVolume()``                     
``player.isAdInProgress()``                        
``player.getCurrentIndex()``               
``player.getPlaylist()``                     
``player.getSource()``            
``player.getVideo()``        

### Objc:  
``[self.player isFullscreen];`` 	
``[self.player isPlayerInView];`` 			
``[self.player getCurrentTime];`` 			
``[self.player getDuration];`` 			
``[self.player getMuted];`` 		     
``[self.player getVolume];`` 		     
``[self.player isAdInProgress];`` 		    	
``[self.player getCurrentIndex];`` 		     
``[self.player getPlaylist];`` 		     
``[self.player getSource];`` 		    			     
``[self.player getVideo];`` 		    			     


## BridSDK player setters methods:

### Swift:  
``player?.setVolume(<volume: Float>)``			
``player.play(by: <Int>)``			
``player.setVideo(<video: VideoData>)`` 					
``player.setVideoUrl(<url: String>)`` 				
``player.setPlaylist(<url: URL>)`` 					
``player.setAd(<ad: [AdData]>)``               
``player.playAd(<ad: AdData>)``                     
``player.playAdTagUrl(<url: URL>)``                        
``player.setAdMacros(<macros: AdMacros>)``               
``player.autoHideControls(<isHidden: Bool>)``         
``player.setPrebidForMidroll(adTagUrl: "<yourTagurl>")``      
``player.setPrebidForPostroll(adTagUrl: "<yourTagurl>")``        

### Objc:  
``[self.player setVolume:<volume: Float>];`` 	
``[self.player playByIndex:<Int>];`` 			
``[self.player setVideo:<video: VideoData>];`` 			
``[self.player setVideoUrl:<url: String>];`` 			
``[self.player setPlaylist:<url: URL>];`` 		     
``[self.player setAd:<ad: [AdData]>];`` 		     
``[self.player playAd:<ad: AdData>];`` 		    	
``[self.player playAdTagUrl:<url: URL>];`` 		     
``[self.player setAdMacros:<macros: AdMacros>];`` 		     
``[self.player autoHideControls:<isHidden: Bool>];``  
``[self.player setPrebidForMidroll:(adTagUrl: "<yourTagurl>")];``      
``[self.player setPrebidForPostroll:(adTagUrl: "<yourTagurl>")];``  

## Set BVPlayer to fullscreen:

### Swift:  
``player?.setToFullscreen()``			


### Objc:  
``[self.player setToFullscreen];``


## When rotate device auto fullscreen is always on, for disable it:

### Swift:  
``player?.autoFullscreen(whenRotating: false)``			


### Objc:  
``[self.player autoFullscreenWhenRotating:NO];``

## Initialize the Brid player with a prebid object:  

For prebid implementations, you need to create an ad object with an ad url that you get as a response from your prebid server. We also provide you with an option to create a custom video object and initialize a Brid player with a prebid ad tag url.
For playlists, prebid ads will be initalized for all videos present in the list.

### Swift:  
 
##### Create Ad Object: 

``let mobile  = Mobile(mobile: "<id>", url: "<yourAdTagUrlFromPrebid>")``	
``var mobileArray = [Mobile]()``  	
``var adArray = [AdData]()``  	   
``mobileArray.append(mobile!)``  	
``let adPod = [mobileArray]``  

For preroll set adType to "0", for midroll set adType to "1", for postroll set adType tp "2"

``let adData = AdData(adType: "1",
                            overlayStartAt: nil,
                            overlayDuration: nil,
                            adTimeType: nil,
                            cuepoints: nil,
                            mobile: adPod)``  
``adArray.append(adData!)`` 

##### Initialize player with prebid:
							
``player = BVPlayer(prebid: adArray, playerID: playerID, forVideoID: videoID, for: yourView)`` 

##### Create Video Object:

``let source = Source()``  
``source.sd = "<yourVideoUrl>"``  
``let videoObject = VideoData(video: <videoID>,
                                    duration: "<durationVideo>",
                                    ageGateId: nil,
                                    name: "nameOfVideo",
                                    _description: "<decription>",
                                    image: "<image>",
                                    thumbnail: "<imageThumbnail>",
                                    clickthroughUrl: nil,
                                    thumb: "<imageThumb>",
                                    publish: nil,
                                    mimeType: nil,
                                    webp: nil,
                                    monetize: nil,
                                    liveimage: nil,
                                    secureUrl: nil,
                                    source: source,
                                    tracks: nil)``  


##### Initialize player prebid with custom video object:

``player = BVPlayer(customVideoWithPrebid: adArray, playerID: playerID, videoDataArray: arrayVideo, for: yourView)``  

##### Initialize single player with custom Video object:

``player = BVPlayer(singleVideo: videoObject, playerID: <playerID>, for: <yourView)``

##### Initialize playlist player with custom Video objects:

``player = BVPlayer(playlist: <arrayVideo>, playerID: <playerID>, for: <yourView)``

### Objc:  

##### Create Ad Object: 

``Mobile *mobile = [[Mobile alloc] initWithMobile:"<id>" url:"<yourAdTagUrlFromPrebid>"]``	
``NSMutableArray<Mobile*> *mobileArray = [[NSMutableArray alloc] init];``  	
``NSMutableArray<AdData*> *adArray = [[NSMutableArray alloc] init];``  
``[mobileArray addObject:mobile];``  	
``NSMutableArray<mobileArray*> *adPod = [[NSMutableArray alloc] init];`` 

For preroll set adType to "0", for midroll set adType to "1", for postroll set adType tp "2"

``AdData *adData = [[AdData alloc] initWithAdType:"1" overlayStartAt:nil overlayDuration:nil adTimeType:nil cuepoints:nil mobile:adPod];``  
``[adArray addObject:adData];``  

##### Initialize player with prebid:
							
``player = [[BVPlayer alloc] initPrebid:adArray playerID:<playerID> forVideoID:<videoID> forView:<yourView>];``   

##### Create Video Object:

``Source *source = [[Source alloc] init];``  
``source.sd = @"<yourVideoUrl>"``  
``VideoData *videoObject = [[VideoData alloc] initWithVideo:<videoID> duration:"<durationVideo>" ageGateId:nil name:"nameOfVideo" _description:"<decription>" image:"<image>" thumbnail:"<imageThumbnail>" clickthroughUrl:nil thumb:<"<imageThumb>"> publish:nil mimeType:nil webp:nil monetize:nil liveimage:nil secureUrl:nil source:source tracks:nil];``    


##### Initialize player prebid with custom video object:

``player = [[BVPlayer alloc] initCustomVideoWithPrebid:<adArray> playerID:<playerID> videoDataArray:<arrayVideo> forView:<yourView>]``


##### Initialize single player with custom Video object:

``player = [[BVPlayer alloc] singleVideo:<videoObject> layerID: <playerID>, for: <yourView]``

##### Initialize playlist player with custom Video objects:

``player = [[BVPlayer alloc] playlist:<arrayVideo> layerID: <playerID>, for: <yourView]``

---

## Requirements

	•	XCode10 or newer
	•	iOS 11.0+
	•	CocoaPods


